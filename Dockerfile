# syntax = docker/dockerfile:1.0-experimental
FROM node:14.15 AS builder

WORKDIR /app
COPY package.json .
# COPY package-lock.json* .
RUN --mount=type=cache,target=/var/cache/apt apt-get update \
        && apt-get install -y build-essential python \
        && apt-get install musl-dev -y \
        && apt-get install nano \
        && ln -s /usr/lib/x86_64-linux-musl/libc.so /lib/libc.musl-x86_64.so.1 \
        && yarn
#RUN npm i -g --unsafe-perm grpc-tools # only for dev

#FROM node:14.15
WORKDIR /app
COPY . .
#COPY --from=builder /app/node_modules /app/node_modules
RUN chmod -R 777 files \
        && chmod -R 777 fileserver \
        && chown node:node .
USER node
EXPOSE 8080
CMD [ "node", "index.js" ]